<?php
namespace Drupal\test_by_contenttype;

use Drupal\test_by_contenttype\Test;
/**
 * Test the semantic structure of the HTML source of the current page
 *
 * PHP version 7
 *
 * @category HTML
 * @package  TestByContenttype
 * @author   Zef Oudendorp <zef@kees-tm.nl>
 * @license  MIT
 * @link     https://packagist.org/packages/keestm/test-by-contenttype
 */
class TestSemanticStructure extends Test
{
    /*
     * Test the semantic structure of the HTML source of the current page
     *
     * @return void
     */
    public function __construct($domain)
    {       

        //Use URL per template specific testing!
        $test_uri = explode("?", $_SERVER["REQUEST_URI"])[0];
        $table_headers = "<th>".$domain.$test_uri."</th>";
        $table_data = "<td>";
        $html_source = "";//this variable will be filled after the get_dom_obj function!
        $dom_obj = $this->_getDomObj($domain, $test_uri, $html_source);
        //Check semantic structure
        $table_data.= $this->_checkStructureHeadings($dom_obj);
        $table_data.= "</td>";
        $table = "<table cellpadding='10' border='1'><tr>".$table_headers."</tr><tr>".$table_data."</tr></table>";
        echo $table;
        die();
    }

    /**
     * Make the semantic structure of the HTML layout visible
     *
     * @param object $dom_obj of the entire HTML source of the page we're on
     *
     * @return string HTML indented list representing the semantic structure of the page we're on
     */
    private function _checkStructureHeadings($dom_obj)
    {
        //HTML5 elements which require headers
        $heading_requiring_elements = array("header", "section", "article", "nav", "aside", "footer");
        $structure = $this->_findStructure($dom_obj, $heading_requiring_elements);
        return "<h2>Semantic structure</h2><ul>".$this->_showStructure($structure)."</ul>";
    }

    /**
     * Recursively check the source for semantic structure
     *
     * @param object $dom_obj of the entire HTML source of the page we're on
     * @param array $heading_requiring_elements HTML5 elements which require headers
     *
     * @return array containing the semantic structure of the page we're on
     */
    private function _findStructure($dom_obj, $heading_requiring_elements)
    {
        $headings = array();
        $sub_structure = array();
        $found_elements = array();
        if ($dom_obj->hasChildNodes()) {
            foreach ($dom_obj->childNodes as $child_node) {
                $found_element = array();
                if (!preg_match("#h\d+#i", $child_node->nodeName)) { //not a header!
                    if (in_array($child_node->nodeName, $heading_requiring_elements)) {//we're looking for this element!
                        $element_found = true;
                        $found_element["element"] = $child_node->nodeName;
                        $found_element["selector"] = ($child_node->getAttribute("id")? "#".$child_node->getAttribute("id"):($child_node->getAttribute("class")? ".".$child_node->getAttribute("class") : ""));
                        //now let's find it's header inside!
                        $element_sub_structure = $this->_findStructure($child_node, $heading_requiring_elements);
                        if (!empty($element_sub_structure)) {
                            $found_element["sub_structure"] = $element_sub_structure;
                        }
                        $found_elements[] = $found_element;
                    } else {//NOT the element we are looking for, but maybe there's one inside?
                        $found_structure = $this->_findStructure($child_node, $heading_requiring_elements);
                        if (!empty($found_structure)) {
                            $sub_structure[] = $found_structure;
                        }
                    }
                } elseif (preg_match("#h\d+#i", $child_node->nodeName)) { //this is a header!
                    $headings[] = array("selector" => $child_node->nodeName, "value" => $child_node->nodeValue);
                }
            }
        }
        $structure = array();
        if (count($headings) > 0) {
            $structure["headings"] = $headings;
        }
        if (count($found_elements) > 0) {
            $structure = array_merge($structure, $found_elements);
        }
        if (count($sub_structure) > 0) {
            if (count($sub_structure) == 1) {
                $sub_structure = $sub_structure[0];
            }
            $structure = array_merge($structure, $sub_structure);
        }
        return $structure;
    }

    /**
     * Recursively transform found semantic structure array to an indented list
     *
     * @param array $structure represents the semantic structure of the HTML layout as returned by_findStructure
     *
     * @return string HTML indented list representing the semantic structure of the page we're on
     */
    private function _showStructure($structure)
    {
        $list = "";
        if (is_array($structure)) {
            foreach ($structure as $key => $node) {
                if (isset($node["element"]) && isset($node["selector"])) {
                    $list.= "<li style='color:green'>";
                    $list.= $node["element"]."<i>".$node["selector"]."</i>";
                    $list.= "<ul>";
                    if (isset($node["sub_structure"]["headings"])) {
                        // $list.= "<li>Single heading:</li>";
                        $list.= $this->_showStructure($node);
                    } elseif (isset($node["sub_structure"][0]["headings"])) {
                        // $list.= "<li>Multiple headings:</li>";
                        $list.= $this->_showStructure($node["sub_structure"]);
                    } elseif (isset($node["sub_structure"][0])) {
                        // $list.= "<li>More substructure:</li>";
                        $list.= $this->_showStructure($node["sub_structure"]);
                    }
                    $list.= "</ul>";
                    $list.= "</li>";
                } elseif ("headings" == $key && isset($node[0])) {
                    foreach ($node as $heading) {
                        if (isset($heading["selector"])) {
                            $list.= "<li style='color:green'>".$heading["selector"]." <i>".($heading["value"]? $heading["value"] : "-empty-")."</i></li>";
                        }
                    }
                    // $list.= "<li>Elements after heading:</li>";
                    $list.= $this->_showStructure($node);
                } else {
                    // $list.= "<li>More elements:</li>";
                    $list.= $this->_showStructure($node);
                }
            }
        }
        return $list;
    }
}