<?php
namespace Drupal\test_by_contenttype;

/**
 * Check HTML source accessibility
 *
 * PHP version 7
 *
 * @category Testing
 * @package  TestByContenttype
 * @author   Zef Oudendorp <zef@kees-tm.nl>
 * @license  MIT
 * @link     https://packagist.org/packages/keestm/test-by-contenttype
 */

 class TestAccessibility
 {
    protected $accessibility_checker_webservice_id = "";
    protected $accessibility_guides = "WCAG2-AA";

    function __construct($domain, $parameters)
    {
        if (isset($parameters['accessibility_webservice_id']) && !empty($parameters['accessibility_webservice_id'])) {
            $this->accessibility_checker_webservice_id = $parameters['accessibility_webservice_id'];
        }
        if (isset($parameters['accessibility_guides']) && !empty($parameters['accessibility_guides'])) {
            $this->accessibility_guides = implode(",", $parameters['accessibility_guides']);
        }
        // Override accessibility_guides with get value if present
        if (isset($_GET["accessibility_guides"]) && is_numeric($_GET["accessibility_guides"])) {
            $this->accessibility_guides = $_GET["accessibility_guides"];
        }
        //Use URL per template specific testing!
        $test_uri = explode("?", $_SERVER["REQUEST_URI"])[0];
        $table_headers = "<th>".$domain.$test_uri."</th>";
        $table_data = "<td>";
        $table_data.= $this->_accessabilityChecker($domain.$test_uri);
        $table_data.= "</td>";
        $table = "<table cellpadding='10' border='1'><tr>".$table_headers."</tr><tr>".$table_data."</tr></table>";
        echo $table;
        die();
    }

    /**
     * Check accessability of the HTML  source
     * Does not work when a cookiewall is present!
     *
     * @param string $uri of the page we're on
     *
     * @return string HTML feedback list
     */
    private function _accessabilityChecker($uri)
    {
        if (!empty($this->accessibility_checker_webservice_id)) {
            $result = $this->_curl("https://achecker.ca/checkacc.php?uri=".$uri."&id=".$this->accessibility_checker_webservice_id."&output=rest&guide=".$this->accessibility_guides);
            $xml = simplexml_load_string($result, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $results = json_decode($json, true);
            $status = $results["summary"]["status"];
            $guideline = (is_array($results["summary"]["guidelines"]["guideline"])? implode(", ", $results["summary"]["guidelines"]["guideline"]) : $results["summary"]["guidelines"]["guideline"]);
            $message = "<h2>Accessability (guideline ". $guideline .") status: <span style='color:".("FAIL" == $status? "red":"green")."'>".$status."</span></h2>";
            $messages = "";
            if (isset($results["results"]["result"])) {
                if (isset($results["results"]["result"][0])) {
                    foreach ($results["results"]["result"] as $result) {
                        if ("Error" == $result["resultType"]) {
                            $messages.= "<li style='color:red'>".$result["errorMsg"].": <i>".htmlspecialchars($result["errorSourceCode"])."</i></li>";
                        }
                    }
                } else {
                    if ("Error" == $results["results"]["result"]["resultType"]) {
                        $messages = "<li style='color:red'>".$results["results"]["result"]["errorMsg"].": <i>".htmlspecialchars($results["results"]["result"]["errorSourceCode"])."</i></li>";
                    }
                }
            }
            if ($messages) {
                $message.= "<ul>".$messages."</ul>";
            }
            return $message;
        }
    }
}