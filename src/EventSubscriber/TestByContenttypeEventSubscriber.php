<?php
namespace Drupal\test_by_contenttype\EventSubscriber;

/**
 * @file
 * Contains \Drupal\my_event_subscriber\EventSubscriber\MyEventSubscriber.
 */

use Drupal;
use DOMDocument;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\test_by_contenttype\TestHtml;
use Drupal\test_by_contenttype\ListTestUris;
use Drupal\test_by_contenttype\TestAccessibility;
use Drupal\test_by_contenttype\TestSemanticStructure;
use Drupal\test_by_contenttype\CustomHtmlCheck;

/**
 * Event Subscriber MyEventSubscriber.
 */
class TestByContenttypeEventSubscriber implements EventSubscriberInterface
{

    protected $testHtml;

    /**
     * Code that should be triggered on event specified
     */
    public function onRespond(FilterResponseEvent $event)
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $domain = "http".((isset($_SERVER["HTTPS"]) && "on" == $_SERVER["HTTPS"])? "s" : "")."://".$_SERVER["SERVER_NAME"];
        $config = \Drupal::config('test_by_contenttype.settings');
        
        if(isset($_GET['test_by_contenttype']) && 1 == \Drupal::currentUser()->id()) {
            new ListTestUris($domain, $config->get('accessibility_webservice_id'));
        }

        if(isset($_GET['test_accessibility']) && 1 == \Drupal::currentUser()->id() && $config->get('accessibility_webservice_id')) {
            $parameters = array(
                'accessibility_webservice_id' => $config->get('accessibility_webservice_id'),
                'accessibility_guides' => $config->get('accessibility_guides'),
            );
            new CheckAccessibility($domain, $parameters);
        }

        if(isset($_GET['test_semantic_structure']) && 1 == \Drupal::currentUser()->id()) {
            new TestSemanticStructure($domain);
        }

        if(isset($_GET['custom_html_check']) && 1 == \Drupal::currentUser()->id()) {
            $parameters = array(
                'check_nav_active_state' => $config->get('check_nav_active_state'),
                'check_meta_msapplication' => $config->get('check_meta_msapplication'),
                'check_meta_og' => $config->get('check_meta_og'),
                'check_form_validation_classes' => $config->get('check_form_validation_classes')
            );
            new CustomHtmlCheck($parameters, $domain);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        // For this example I am using KernelEvents constants (see below a full list).
        $events[KernelEvents::RESPONSE][] = ['onRespond'];
        return $events;
    }
}
