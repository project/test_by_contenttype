<?php
namespace Drupal\test_by_contenttype;

use DOMDocument;

/**
 * Test class to extend
 *
 * PHP version 7
 *
 * 
 * @category HTML
 * @package  TestByContenttype
 * @author   Zef Oudendorp <zef@kees-tm.nl>
 * @license  MIT
 * @link     https://packagist.org/packages/keestm/test-by-contenttype
 */
class Test
{

    /**
     * Valide HTML using W3C validation API
     *
     * @param string  $url we're about to cURL
     * @param string  $data_string data to post
     * @param boolean $get_http_status to only return the HTTP status of the url
     *
     * @return mixed
     *
     */
    public function _curl($url, $data_string = "", $get_http_status = "")
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, ($get_http_status? true:false)); //still returning header?
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //Disable SSL verification!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        //post fields if present
        if (!empty($data_string)) {
            $http_header = array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            );
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $http_header);
        }
        if ($get_http_status) {
            curl_setopt($curl, CURLOPT_NOBODY, true);
        }
        //Expressionengine uses these options
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
        $return_data = curl_exec($curl);
        // Check for errors and display the error message
        if ($errno = curl_errno($curl) || $return_data === false) {
            $error_message = curl_strerror($errno);
            echo "cURL error ({$errno}):\n {$error_message} for ".$url."<br />";
        }
        if ($get_http_status) {
            $return_data = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        }
        curl_close($curl);
        return $return_data;
    }

    /**
     * Get DOM object & HTML source!
     *
     * @param string $domain of the site we'r e on
     * @param string $test_uri of the page we're on
     * @param string $html_source of the page we're on, will be given a value inside this function!
     *
     * @return object domObject of the page we're on
     */
    public function _getDomObj($domain, $test_uri, &$html_source)
    {
        //Is this URL available?
        if ($this->_isDeadLink($domain, $test_uri)) {
            return false;
        } else {
            $html_source = $this->_curl($domain.$test_uri, false);
            libxml_use_internal_errors(true);
            $dom_obj = new DOMDocument();
            $dom_obj->loadHTML($html_source);
            return $dom_obj;
        }
    }

    /**
     * Check if the link returns header 200, 301 or 302
     *
     * @param string $domain of the site we're on
     * @param string $url of the link which we're about to check
     *
     * @return boolean
     *
     */
    public function _isDeadLink($domain, $url)
    {
        //Check if it's a  local or remote (or even protocol relative!) URL so we can see if the content actually exists
        $url = (strstr($url, "http")? "": (preg_match("#^\/\/#", $url)? "https:": $domain)).$url;
        $http_status = $this->_curl($url, false, true);
        return false;
        if (!in_array($http_status, ["200", "301", "302"])) {
            return true;
        } else {
            return false;
        }
    }
}