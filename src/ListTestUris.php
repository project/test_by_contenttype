<?php
namespace Drupal\test_by_contenttype;

/**
 * List Example URIs for all contenttypes & views
 *
 * PHP version 7
 *
 * @category Testing
 * @package  TestByContenttype
 * @author   Zef Oudendorp <zef@kees-tm.nl>
 * @license  MIT
 * @link     https://packagist.org/packages/keestm/test-by=contenttype
 */

 class ListTestUris
 {
    var $accessibility_webservice_id = false;
    function __construct($domain, $accessibility_webservice_id)
    {
        $this->accessibility_webservice_id = $accessibility_webservice_id;
        $default_lang_code = \Drupal::languageManager()->getDefaultLanguage()->getId();
        $database = \Drupal::database();
        $distinct_lang_url_aliases = $this->_getDistinctUrlAliases($database);
        $node_tr = $this->_getNodeTebleRows($distinct_lang_url_aliases, $default_lang_code, $domain);
        $view_tr = $this->_getViewTableRows($database, $default_lang_code, $domain);
        echo "<table cellspacing='10'>";
        echo "<tr>";
        echo "<th align='left'>Language</th>";
        echo "<th align='left'>Content type</th>";
        echo "<th align='left'>Qty</th><th align='left'>Example URI</th>";
        echo "<th></th>";
        echo "<th align='left'>TESTs</th>";
        echo "<th></th>";
        echo "<th></th>";
        echo "<th></th>";
        echo "<th align='left'>".(isset($_GET['type'])? "<a href='?test_by_contenttype'>&laquo Back</a>" : "")."</th>";
        echo "</tr>";
        echo $node_tr;
        echo $view_tr;
        echo "</table>"; 
        exit;
    }

    private function _getViewTableRows($database, $default_lang_code, $domain){
        $tr = "";
        if(!isset($_GET['type'])){
            $views = $database->query("SELECT `path` FROM router WHERE `name` LIKE 'view\.%' AND `path` NOT LIKE '\/admin\/%'");
            foreach($views as $view){
                $link_content_data = $database->query("SELECT langcode FROM menu_link_content_data WHERE link__uri = 'internal:".preg_replace("#{.*}?#", "", $view->path)."'");
                $langcode = "?";
                foreach($link_content_data as $link_content){
                    $langcode = $link_content->langcode;
                }
                $uri = ($langcode != "?"? ($langcode != $default_lang_code? "/".$langcode : "") : "").preg_replace("#{.*}?#", "", $view->path);
                $tr.= $this->_getTableRow($uri, $domain, $langcode, "<strong>view</strong?", 1);
            }
        }
        return $tr;
    }

    private function _getNodeTebleRows($distinct_lang_url_aliases, $default_lang_code, $domain){
        $i = 1;
        $tr = "";
        foreach($distinct_lang_url_aliases as $langcode => $distinct_url_aliases){
            foreach($distinct_url_aliases as $type => $url_alias){
                $uri = ($langcode != $default_lang_code? "/".$langcode : ""). $url_alias['alias'];
                $type = (isset($_GET['type'])? $_GET['type'] : $type);
                $qty = (isset($_GET['type'])? $i : $url_alias['qty']);
                $tr.= $this->_getTableRow($uri, $domain, $langcode, $type, $qty);
                $i++;
            }
        }
        return $tr;
    }

    private function _getTableRow($uri, $domain, $langcode, $type, $qty){
        $more = "";
        if(!isset($_GET['type'])){
            $more = ($qty>1? "<a href='?test_by_contenttype&type=".$type."&langcode=".$langcode."'>More &raquo</a>" : "");
        }
        $tr = "<tr>";
        $tr.= "<td>".$langcode."</td>";
        $tr.= "<td>".$type."</td>";
        $tr.= "<td>".$qty."</td>";
        $tr.= "<td><a href='".$uri."' target='_blank'>".$uri."</a></td>";
        $tr.= "<td>".(isset($more)? $more : "")."</td>";
        $tr.= "<td><a href='https://validator.w3.org/nu/?doc=".$domain.$uri."' target='_blank'>W3 validate &raquo;</a></td>";
        $tr.= "<td><a href='".$uri."?test_semantic_structure' target='_blank'>Semantic structure &raquo;</a></td>";
        $tr.= "<td><a href='".$uri."?custom_html_check' target='_blank'>Custom check &raquo;</a></td>";
        if($this->accessibility_webservice_id){
            $tr.= "<td><a href='".$uri."?test_accessibility' target='_blank'>Accessibility &raquo;</a></td>";
        }
        $tr.= "<td><a href='https://developers.google.com/speed/pagespeed/insights/?hl=nl&url=".$domain.$uri."' target='_blank'>Speed &raquo;</a></td>";
        $tr.= "</tr>";
        return $tr;
    }

    private function _getDistinctUrlAliases($database){
        $languages = \Drupal::languageManager()->getLanguages();
        $distinct_lang_url_aliases = array();
        foreach($languages as $language){
            $langcode = $language->getId();
            if(isset($_GET['langcode']) && $_GET['langcode'] != $langcode){
                continue;
            }
            $node_type_langcode = false;
            $nodes = $database->query("SELECT nid, `type` FROM node".(isset($_GET['type'])? " WHERE `type` = '".$_GET['type']."'" : ""));
            foreach($nodes as $node){
                if($node_type_langcode != $node->type.$langcode || isset($_GET['type'])){
                    $node_type_langcode = $node->type.$langcode;                    
                    $uniquify = (isset($_GET['type'])? $node->nid : "");
                    $distinct_lang_url_aliases[$langcode][$node->type.$uniquify]['qty'] = 1;
                    $url_aliases = $database->query("SELECT alias, langcode FROM url_alias WHERE source = '/node/".$node->nid."'".(isset($_GET['langcode'])? " AND langcode = '".$_GET['langcode']."'" : " AND langcode = '".$langcode."'")." ORDER BY langcode DESC");
                    foreach($url_aliases as $url_alias){
                        $distinct_lang_url_aliases[$langcode][$node->type.$uniquify]["alias"] = $url_alias->alias;
                    }
                    //No alias found?
                    if(!isset($distinct_lang_url_aliases[$langcode][$node->type.$uniquify]['alias'])){
                        $distinct_lang_url_aliases[$langcode][$node->type.$uniquify]["alias"] = "/node/".$node->nid;
                    }
                }else{
                    if(isset($distinct_lang_url_aliases[$langcode][$node->type]['alias'])){
                        $distinct_lang_url_aliases[$langcode][$node->type]['qty']++;
                    }
                }
            }
        }
        return $distinct_lang_url_aliases;
    }
 }